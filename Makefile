# To build external modules, you must have a prebuilt kernel available
# that contains the configuration and header files used in the build.
# go in the kernel directory and do a
#       make oldconfig; make scripts; make prepare
#
# The list of targets is derived from obj-m
# and then the corresponding foo-objs

# Empty if you don't want a debug build.
DEBUG=-DDEBUG -g

CONFIG_TCP_NOORDWIJK:=m

obj-$(CONFIG_TCP_NOORDWIJK) = tcp_noord.o

# The following commands are needed to build the modules as out-of-tree, in
# fact the kernel sources path must be specified.

# Additional compile flags (e.g. header location)
M:=$(PWD)
EXTRA_CFLAGS := -I$(M) $(DEBUG)

# We use KSRC for the kernel configuration and sources.
# If the sources are elsewhere, then use SRC to point to them.
KSRC ?= /lib/modules/$(shell uname -r)/build
SRC ?= $(KSRC)

# extract version number and filter with the available patches.
LIN_VER = $(shell grep LINUX_VERSION_CODE $(KSRC)/include/generated/uapi/linux/version.h | awk '{printf "%03x%02d", $$3/256, $$3%256} ')

all:	build

build:
	make -C $(KSRC) M=$(PWD) CONFIG_TCP_NOORDWIJK=m \
		EXTRA_CFLAGS='$(EXTRA_CFLAGS)' \
		modules
	@ls -l `find . -name \*.ko`

test:
	@echo "version $(LIN_VER)"

clean:
	-@ make -C $(KSRC) M=$(PWD) clean
	-@ (rm -rf *.orig *.rej *.ko *.o *.~)
